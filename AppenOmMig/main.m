//
//  main.m
//  AppenOmMig
//
//  Created by Emilie Myhrman on 2016-01-31.
//  Copyright © 2016 Emilie Myhrman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
